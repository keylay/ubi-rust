# SPDX-FileCopyrightText: 2023 John Moon <john@jmoon.dev>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

ARG BASE_REGISTRY=registry.access.redhat.com
ARG BASE_IMAGE=ubi9/ubi
ARG BASE_TAG=9.4

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

ENV CARGO_HOME=/opt/rust/src/.cargo
ENV PATH=/opt/rust/src/.cargo/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

SHELL [ "/bin/bash", "-o", "pipefail", "-c"]

# hadolint ignore=DL3003,DL3041
RUN dnf update --setopt=tsflags=nodocs -y && \
    dnf install -y --nodocs gcc \
                            libpq{,-devel} \
                            zlib-devel \
                            openssl-devel \
                            mariadb-connector-c-devel \
                            xz && \
    dnf clean all

# TODO: Remove custom cargo-license branch when/if PR is merged:
# https://github.com/onur/cargo-license/pull/75
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs \
        | sh -s -- --default-toolchain none -y && \
    rustup toolchain install stable && \
    cargo install cargo-audit \
                  cargo-outdated \
                  cargo-machete && \
    cargo install --git https://github.com/JohnCMoon/cargo-license.git --branch output-opt && \
    rm -rf "${CARGO_HOME}"/registry/{cache,src}

SHELL [ "/bin/bash", "-l", "-c" ]
ENTRYPOINT [ "/bin/bash", "-l", "-c" ]
