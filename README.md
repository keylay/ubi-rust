<!--
SPDX-FileCopyrightText: 2023 John Moon <john@jmoon.dev>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# UBI Rust

This project generates the UBI build container for the Keylay project.
It includes the latest stable toolchain as well as the additional development
libraries and analysis programs used in the Keylay pipeline.